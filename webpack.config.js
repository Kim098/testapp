const HtmlBundlerPlugin = require("html-bundler-webpack-plugin");
const Handlebars = require('handlebars');
const path = require("path");
const fs = require('fs');
const { merge } = require("webpack-merge");

const PATHS = {
  
  partials: path.join(__dirname, 'src/views/partials/'),
};

//Handlebars helpers used in templates.
const handlebarHelpers = {

  include: (filename, options, args) => {
    const tmplExt = '.html';
    const { ext } = path.parse(filename);
    if(!ext) filename += tmplExt;

      try {
        const templatePath = path.join(PATHS.partials, filename);
        const template = fs.readFileSync(templatePath, 'utf-8');
        const data = options.name === 'include' 
        ? {...options?.hash, ...options.data?.root} 
        : {...options, ...args?.data?.root};
        const html = Handlebars.compile(template)(data);
        return new Handlebars.SafeString(html); 
      } catch (error) {
        return new Handlebars.SafeString('');
      }
    },
  
  limit: (arr, limit) => {
    if(!Array.isArray(arr)) { return []; }
    return arr.slice(0, limit);
  }
}//---/ const handlebarHelpers --

//Register handlebars helpers
for(const helper in handlebarHelpers) {
  Handlebars.registerHelper(helper, handlebarHelpers[helper]);
}

//Project config data
const projectData = merge (
  {webRoot: ''},
  {config: require(path.join(__dirname, 'src/data/config.json'))},
);

module.exports = {
  mode: "none",
  // entry: "./src/index.js",
  output: {
    path: path.resolve(__dirname, "public"),
    filename: "bundle.js",
  },
 
  resolve: {
    alias: {
      '@images': path.resolve(__dirname,'src/assets/images'),
      '@styles': path.resolve(__dirname, 'src/assets/scss'),
      '@scripts': path.resolve(__dirname, 'src/assets/js'),

    },
    extensions: ['.js', '.jsx'],  // Allow importing JS and JSX files without specifying extension
  },
  plugins: [
    new HtmlBundlerPlugin({
      template: './src/profile.html',
      entry: {
        index: {
          import: 'src/profile.html',
        },
        restaurantindex: {
          import: 'src/views/delicious/index.html',
        },
        dayindex: {
          import: 'src/views/day/day-index.html',
        },
        UCindex: {
          import: 'src/views/up-construction/index.html',
        },
        UCabout: {
          import: 'src/views/up-construction/about.html',
        },
          UCservices: {
            import: 'src/views/up-construction/services.html',
          },
          UCprojects: {
            import: 'src/views/up-construction/projects.html',
          },
          UCprojectDetails: {
            import: 'src/views/up-construction/project-details.html',
          },
          UCcontact: {
            import: 'src/views/up-construction/contact.html',
          },
        Eternaindex: {
          import: 'src/views/eterna/index.html',
        }
        
      },
      js: {
        filename: "assets/js/[name].[contenthash:8].js",  // Optional: Output JS file with content hash
      },
      css: {
        filename: 'assets/css/[name].[contenthash:8].css',
      },

      loaderOptions: {
        preprocessor: (content) => Handlebars.compile(content)(projectData),
      }
    }),//--/HtmlBundlerPlugin --
  ],

  module: {
    rules: [
      {
        test: /\.(sass|scss|css)$/,
        use: [
          'css-loader', 'postcss-loader',
          {
            loader: 'sass-loader',
            options: {
              sassOptions: {
                quietDeps: true,
              }
            }
          }
        ]
      },
      {
        test: /\.(png|jpe?g|webp|ico|svg)$/i,
        type: 'asset/resource',
        generator: {
          filename: 'assets/img/[name].[hash:8][ext]',
        },
      },

      {
        test: /\\.js$/,
        loader: "babel-loader",
        exclude: "/node_modules/",
      },

      {
        test: /\.(js|jsx)$/,   // Regex to target both JS and JSX files
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-env', '@babel/preset-react'],
          },
        },
      },
      
    ],
  },

  devServer: {
    static: {
      directory: path.join(__dirname, './public'), // or './public' depending on your setup
    },
    compress: true,
    port: 8080,
    historyApiFallback: true, // For handling React Router routes
  }
  
 
};



