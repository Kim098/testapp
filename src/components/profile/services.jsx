'use strict';

const e = React.createElement;

class Services extends React.Component {
  constructor(props) {
    super(props);
    this.state = { liked: false };
  }

  render() {
    
    
    return (
        <section id="services" class="services section">
       
           {/* <div class="like_button_container" data-commentid="2"></div> */}
       
           
           {/* <!-- Section Title --> */}
           <div class="container section-title" data-aos="fade-up">
             <h2>Services</h2>
             <p>Magnam dolores commodi suscipit. Necessitatibus eius consequatur ex aliquid fuga eum quidem. Sit sint consectetur velit. Quisquam quos quisquam cupiditate. Et nemo qui impedit suscipit alias ea. Quia fugiat sit in iste officiis commodi quidem hic quas.</p>
           </div>
           {/* <!-- End Section Title --> */}
       
           <div class="container">
       
             <div class="row gy-4">
       
               <div class="col-lg-4 col-md-6 service-item d-flex" data-aos="fade-up" data-aos-delay="100">
                 <div class="icon flex-shrink-0"><i class="bi bi-briefcase"></i></div>
                 <div>
                   <h4 class="title"><a href="service-details.html" class="stretched-link">Template Customization</a></h4>
                   <p class="description">
                   Adjusting pre-designed templates to fit a client's specific needs, including branding, color schemes, fonts, and layout tweaks.
                   </p>
                 </div>
               </div>
               {/* <!-- End Service Item --> */}
       
               <div class="col-lg-4 col-md-6 service-item d-flex" data-aos="fade-up" data-aos-delay="200">
                 <div class="icon flex-shrink-0"><i class="bi bi-card-checklist"></i></div>
                 <div>
                   <h4 class="title"><a href="service-details.html" class="stretched-link">Responsive Template Design</a></h4>
                   <p class="description">
                   Offering ready-made templates that are mobile-friendly and optimized for various screen sizes, ensuring smooth functionality on all devices.
                   </p>
                 </div>
               </div>
               {/* <!-- End Service Item --> */}
       
               <div class="col-lg-4 col-md-6 service-item d-flex" data-aos="fade-up" data-aos-delay="300">
                 <div class="icon flex-shrink-0"><i class="bi bi-bar-chart"></i></div>
                 <div>
                   <h4 class="title"><a href="service-details.html" class="stretched-link">Portfolio Template Setup</a></h4>
                   <p class="description">
                    Providing templates designed for portfolios, allowing clients (artists, freelancers, or agencies) to showcase their work professionally and attractively.
                    </p>
                 </div>
               </div>
               {/* <!-- End Service Item --> */}
       
               <div class="col-lg-4 col-md-6 service-item d-flex" data-aos="fade-up" data-aos-delay="400">
                 <div class="icon flex-shrink-0"><i class="bi bi-binoculars"></i></div>
                 <div>
                   <h4 class="title"><a href="service-details.html" class="stretched-link">Landing Page Template Design</a></h4>
                   <p class="description">
                   Offering pre-designed landing page templates that can be easily customized for marketing campaigns, product launches, or lead generation.


                   </p>
                 </div>
               </div>
               {/* <!-- End Service Item --> */}
       
               <div class="col-lg-4 col-md-6 service-item d-flex" data-aos="fade-up" data-aos-delay="500">
                 <div class="icon flex-shrink-0"><i class="bi bi-brightness-high"></i></div>
                 <div>
                   <h4 class="title"><a href="service-details.html" class="stretched-link">CMS-Ready Templates</a></h4>
                   <p class="description">
                    Providing templates designed for popular content management systems (CMS) like WordPress, Shopify, or Joomla, allowing for easy installation and management.
                    </p>
                 </div>
               </div>
               {/* <!-- End Service Item --> */}
       
               <div class="col-lg-4 col-md-6 service-item d-flex" data-aos="fade-up" data-aos-delay="600">
                 <div class="icon flex-shrink-0"><i class="bi bi-calendar4-week"></i></div>
                 <div>
                   <h4 class="title"><a href="service-details.html" class="stretched-link">Template-Based Website Redesign</a></h4>
                   <p class="description">
                   Utilizing pre-made templates to give outdated websites a modern look, reducing design time while maintaining high-quality results.
                   </p>
                 </div>
               </div>
               {/* <!-- End Service Item --> */}
       
             </div>
       
           </div>
       
         </section>
      );

  }
}

// Find all DOM containers, and render Like buttons into them.
document.querySelectorAll('.services-container')
  .forEach(domContainer => {
    // Read the comment ID from a data-* attribute.
    const commentID = parseInt(domContainer.dataset.commentid, 10);
    const root = ReactDOM.createRoot(domContainer);
    root.render(
      e(Services, { commentID: commentID })
    );
  });

/**
 * const root = ReactDOM.createRoot(document.getElementById('root'));
    root.render(<h1>Hello, world!</h1>);
 */


















    