import React from 'react';
import PropTypes from 'prop-types';
import data from "../../data/profile.json"; // Metadata only, no direct image paths

const PortfolioItem = ({ imgSrc, title, description, detailsLink }) => {
  // Dynamically import the image using require to get the hashed image file from Webpack
  const imageSrc = require(`../../assets/images/profile-images/portfolio/${imgSrc}`);
  return (
    <div className="col-lg-4 col-md-6 portfolio-item 
    isotope-item filter-business">
      <div className="portfolio-content h-100">
        <img src={imageSrc} className="img-fluid" alt={title} />
        <div className="portfolio-info">
          <h4>{title}</h4>
          <p>{description}</p>
          <a
            href={imageSrc}
            title={title}
            data-gallery="portfolio-gallery-app"
            className="glightbox preview-link"
          >
            <i className="bi bi-zoom-in"></i>
          </a>
          <a href={detailsLink} title="More Details" className="details-link">
            <i className="bi bi-link-45deg"></i>
          </a>
        </div>
      </div>
    </div>
  );
};

PortfolioItem.propTypes = {
  imgSrc: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
  detailsLink: PropTypes.string.isRequired,
};
export default PortfolioItem;

