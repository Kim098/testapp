import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import data from '../../data/profile.json';
import PortfolioItem from './PortfolioItem';
'use strict';

const e = React.createElement;
class Portfolio extends React.Component {
  constructor(props) {

    super(props);
  }

  render() {

  const portfolioContainer = document.getElementById('portfolio-items-container');

      return (
      
        
      
      <>
        {data.map((item, index) => (
          <PortfolioItem
            key={index}
            imgSrc={item.imgSrc}  // Now it's just the file name, not the full path
            title={item.title}
            description={item.description}
            detailsLink={item.detailsLink}
          />
        
          
        ))}
      </>

      );
    }

  }

const portfolioContainer = document.getElementById('portfolio-items-container');
// Render the class component into the container if it exists
if (portfolioContainer) {
  const root = ReactDOM.createRoot(portfolioContainer);

  root.render(
    e(Portfolio)
  );
}
